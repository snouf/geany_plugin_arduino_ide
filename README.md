Arduino IDE in Geany
========================

** WARNING requires geanypy. Geanypy is not available on the latest version of geany. **

Select board, insert library, compile and upload code to Arduino and
combatible board in Geany.

Require
-------------------------

  - [Aduino IDE >= 1.5](http://www.arduino.cc/)
  - [GeanyPy](http://plugins.geany.org/geanypy.html)
  - [pyserial](https://pypi.python.org/pypi/pyserial)
  - optionnaly [GtkTerm](http://gtkterm.feige.net/)


Install
-------------------------

Download [latest.zip](https://gitlab.com/snouf/geany_plugin_arduino_ide/-/archive/master/geany_plugin_arduino_ide-master.zip).

Copy:

  - `plugins/geanypy/plugins/arduino.py` in `~/.config/geany/plugins/`
  - `filedefs/filetypes.Arduino.conf` in `~/.config/geany/filedefs/`
  - `templates/files/sketch.ino` in `~/.config/geany/templates/files/`

Open `filetype_extentions.conf` (Tools → Configuration files):

  - add `Arduino=*.ino;*.pde;` in section `[Extensions]` (see [filetype_extentions.conf.example ligne 9](https://gitlab.com/snouf/geany_plugin_arduino_ide/blob/master/filetype_extensions.conf.example#L9))
  - add `Arduino` in `Programming` section `[Groups]` (see [filetype_extentions.conf.example ligne 76](https://gitlab.com/snouf/geany_plugin_arduino_ide/blob/master/filetype_extensions.conf.example#L76))

Open plugins manager (Tools → Plugins manager) and active "GeanyPy" and
"Arduino IDE"

Open `~/.config/geany/plugins/arduino.conf` and add your arduino IDE
installation path example `dirs = /home/username/arduino-1.8.5` close and
open again geany.


How to use
-------------------------

This plugin add an entry "Arduino" in "Tools" menu. With this you can
select board, port, insert library and other.

For verify and upload code see "construct" menu (or compil and build
buttons in toolbar).

You can run a serial terminal (gtkterm by default) in "construct" menu.


Advances preferences
-------------------------

This plugins search libraries and boards in:

  * On Linux/unix/cygwin: `/usr/local/share/arduino`,`/usr/share/arduino`,
    `~/Arduino`,`~/.arduino`,`~/.arduino15` directories.
  * On windows: FIXME #1
  * On OSX: FIXME #2

You can add it in config file ((usually ~/.config/geany/plugins/arduino.conf))
or environment variable `GEANY_ARDUINO_DIRS` (separator "`,`").

This plugins search packages in :

  * On Linux/unix/cygwin: `~/Arduino`,`~/.arduino`,`~/.arduino15` directories.
  * On windows: FIXME #1
  * On OSX: FIXME #2

You can add it in config file ((usually ~/.config/geany/plugins/arduino.conf))
or with environment variable `GEANY_ARDUINO_PACKAGES_DIRS` (separator "`,`").

You can change "verify", "upload" and "open serial terminal" in menu
"Construct" → "Define construct command".
